#!/bin/sh
set -x
echo "Copying configs..."

mkdir -p /config/.openttd
cp -r /container_conf/. /config/.openttd

cp /run/secrets/openttd/secrets.cfg /config/.openttd

mkdir -p /config/.openttd/save
mkdir -p /config/.openttd/screenshot

chown -R 4101:4101 /config

echo "****** SETTING LISTEN PORT TO ${OPENTTD_PORT} ******"
sed -i "/^server_port = /s/=.*/= ${OPENTTD_PORT}/" /config/.openttd/openttd.cfg 

if [ "$DEPLOY_ENV" == "dev" ]; then
  echo "****** MODIFYING CONFIG FOR DEV ******"
  # prevent server advertisement for dev
  sed -i "/^server_game_type = /s/=.*/= 0/" /config/.openttd/openttd.cfg 
fi